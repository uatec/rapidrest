﻿using Xunit;
using System.Collections.Generic;

namespace RapidRest.Tests
{
    public class TypeExtensionTests 
    {

        public class BaseClass {
            public void Method1() {}
        }

        public class ChildClass : BaseClass {
            public void Method2() {}
        }

        [Fact]
        public void GetDeepMethods() {

            Assert.NotNull(typeof(ChildClass).GetDeepMethods("Method1"));
            Assert.NotNull(typeof(ChildClass).GetDeepMethods("Method2"));
        }

        [Fact]
        public void ImplementedBy() {

            Assert.True(typeof(List<>).ImplementedBy(typeof(List<string>)), "List<string> is found to be a subclass of List<>");
        }

        [Fact]
        public void asdfasdf() {

            Assert.True(typeof(IList<>).ImplementedBy(typeof(IMyStrings)), "IMyStrings is found to be a subclass of List<>");
        }
    }

    public interface IMyStrings : IList<string> {}
}
