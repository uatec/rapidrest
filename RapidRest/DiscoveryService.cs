using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using RapidRest.Configuration;
using RapidRest.Paging;
using RapidRest.Simple;

namespace RapidRest
{
    public class DiscoveryService
    {
        public IEnumerable<Registration> Discover(Assembly assembly)
        {
            var registrations = assembly
                .GetTypes()
                .Where(t => t != typeof(IRepository<>))
                .Where(t => t != typeof(IPagedRepository<>))
                .Where(t => typeof(IRepository<>).ImplementedBy(t))
                .Select(t => new Registration {
                    Name = t.GetInterfaces()[0].GetGenericArguments()[0].Name,
                    RoutePrefix = $"api/v1/{t.GetInterfaces()[0].GetGenericArguments()[0].Name}",
                    RepoType = t,
                    ResourceType = t.GetInterfaces()[0].GetGenericArguments()[0],
                    AuthorizeConfig = t.GetTypeInfo().GetCustomAttribute<Authorize>(),
                    ScopeConfig = t.GetTypeInfo().GetCustomAttribute<ScopedTo>(),
                    Methods = t.GetMethods().Where(m => m.GetParameters().Length == 1)
                        .Select(m => new MethodRegistration {
                            Route = $"/_search/{m.Name}",
                            Method = m
                        })
                }).ToArray();

            return registrations;
        }

        public static string DiscoverIdFieldName(Type t) {
            // TODO: Introduce a standard way of flagging the ID field, then ask the serialiser what it's going to call that field.
            string candidateCSharpFieldName = null;

            foreach ( PropertyInfo field in t.GetProperties()) {
                JsonPropertyAttribute jsonPropertyAttribute = (JsonPropertyAttribute) field.GetCustomAttribute(typeof(JsonPropertyAttribute));
                if ( jsonPropertyAttribute != null )
                {
                    if ( string.Equals(jsonPropertyAttribute.PropertyName, "id", StringComparison.OrdinalIgnoreCase) ) 
                    {
                        candidateCSharpFieldName = field.Name;
                        continue;
                    }
                }
                
                if ( string.Equals(field.Name, "id", StringComparison.OrdinalIgnoreCase) ) {
                    candidateCSharpFieldName = field.Name;
                }
            }

            if ( candidateCSharpFieldName == null ) throw new Exception("Could not find 'id' field on type " + t.FullName);
            
            return candidateCSharpFieldName;
        }
    }
}