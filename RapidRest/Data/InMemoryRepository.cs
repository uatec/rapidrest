using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using RapidRest.Paging;

namespace RapidRest.Data
{
    public class InMemoryRepository<T> : IPagedRepository<T> {

        private readonly string IdFieldName;

        private static IList<T> _data = new List<T>();
        
        private readonly string ScopeValue;
        private readonly string ScopeField;
        
        public InMemoryRepository(RequestContext requestContext) 
        {
            this.ScopeValue = requestContext.ScopeClaim?.Value;
            this.ScopeField = requestContext.ScopeField;
            IdFieldName = DiscoveryService.DiscoverIdFieldName(typeof(T));
        }
    
        public IEnumerable<T> GetAll(PagingInfo pagingInfo)
        {
            return _data
                .Where(i => {
                    if ( this.ScopeField == null ) return true;
                    return getField(i, this.ScopeField) == this.ScopeValue;
                })
                .Skip(pagingInfo.Page * pagingInfo.PageSize)
                .Take(pagingInfo.PageSize)
                .ToList();
        }

        public IEnumerable<T> GetAll()
        {
            return _data
                .Where(i => this.ScopeField == null || getField(i, this.ScopeField) == this.ScopeValue);
        }

        public string Save(string id, T obj)
        {
            var objectId = getId(obj);
            
            if ( id != null && objectId != null &&  id != objectId ) {
                throw new Exception("tried to change the ID of an object");
            }

            id = id ?? objectId ?? Guid.NewGuid().ToString();
            setId(id, obj);

            if ( this.ScopeField != null ) setField(this.ScopeField, this.ScopeValue, obj);

            var existingObject = _data.SingleOrDefault(d => getId(d) == id);


            if ( existingObject != null ) {
                if ( this.ScopeField != null && getField(existingObject, this.ScopeField) != this.ScopeValue) throw new Exception("not authorized but functionality not implemented");
                // TODO: Some kind of merge
                _data.Remove(existingObject);
            }
            _data.Add(obj);

            return id;
        }

        public T Get(string id)
        {
            var item = _data
                .SingleOrDefault(d => getId(d) == id);      

            if ( item == null ) return default(T);

            if ( this.ScopeField != null ) {
                if ( getField(item, this.ScopeField) != this.ScopeValue )  {
                    throw new Exception("not authorized but functionality not implemented");
                } 
            }

            return item;
        }

        private string getId(object obj)
        {
            return getField(obj, IdFieldName);
        }

        private string getField(object obj, string key) 
        {
            return (string) typeof(T).GetProperty(key).GetValue(obj);
        }

        private void setId(string id, object obj)
        {
            setField(IdFieldName, id, obj);
        }

        private void setField(string fieldname, string value, object obj)
        {
            typeof(T).GetProperty(fieldname).SetValue(obj, value);
        }

        public T Delete(string id)
        {
            var objectToDelete = _data.SingleOrDefault(d => getId(d) == id);

            if ( objectToDelete == null ) return default(T);
            
            if ( this.ScopeField != null && getField(objectToDelete, this.ScopeField) != this.ScopeValue) throw new Exception("not authorized but functionality not implemented");
            
            _data.Remove(objectToDelete);
            return objectToDelete;
        }

        public T FindOneBy(string key, object value)
        {
            var rtn = _data
                .Where(d => {
                    return value.Equals(typeof(T).GetProperty(key).GetValue(d));
                })
                .Where(i => this.ScopeField == null || getField(i, this.ScopeField) == this.ScopeValue)
                .FirstOrDefault();

            return rtn;
        }

        public IEnumerable<T> FindBy(string key, object value)
        {
            var rtn = _data
                .Where(d => {
                    return value.Equals(typeof(T).GetProperty(key).GetValue(d));
                })
                .Where(i => this.ScopeField == null || getField(i, this.ScopeField) == this.ScopeValue);

                typeof(T).GetProperties();

            return rtn.ToList();
        }
    }
}