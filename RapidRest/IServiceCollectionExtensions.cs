using System.Collections.Generic;
using System.Reflection;
using Castle.DynamicProxy;
using Microsoft.Extensions.DependencyInjection;
using RapidRest.Configuration;
using RapidRest.Data;
using RapidRest.Eventing;
using RapidRest.Paging;
using RapidRest.Simple;

namespace RapidRest
{ 
    public static class IServiceCollectionExtensions
    {
        public static void RegisterPagedRepository<T, TRepo>(this IServiceCollection services) where TRepo: class
        {
            services.AddScoped(typeof(PagedStore<,>));            
            services.AddScoped(typeof(TRepo), sp => {
                IProxyGenerator proxyGenerator = new ProxyGenerator();

                return proxyGenerator.CreateInterfaceProxyWithoutTarget<TRepo>(
                   (SimpleInterceptor<T>) sp.GetService(typeof(SimpleInterceptor<T>)));
            });
            services.AddScoped(typeof(IPagedRepository<T>), typeof (InMemoryRepository<T>));
            services.AddScoped<SimpleInterceptor<T>>();
            services.AddSingleton(typeof(IEventEmitter<T>), rp => new SimpleEmitter<T>());
            services.AddScoped<RequestContext>();
        }
        
        public static void AddRapidRest(this IServiceCollection services, Assembly assembly)
        {
            services.AddRouting();

            var registrations = new DiscoveryService().Discover(assembly);
            services.AddSingleton<IEnumerable<Registration>>(registrations);
   
            foreach ( var r in registrations )
            {   
                typeof(IServiceCollectionExtensions)
                    .GetMethod("RegisterPagedRepository", BindingFlags.Public | BindingFlags.Static)
                    .MakeGenericMethod(r.ResourceType, r.RepoType)
                    .Invoke(null, new object[] { services });
            }
        }
    }
}