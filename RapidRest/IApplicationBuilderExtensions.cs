using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using RapidRest.Configuration;
using RapidRest.Paging;
using RapidRest.Simple;

namespace RapidRest
{
    public class RequestContext
    {
        public string ScopeField { get; set; }
        public Claim ScopeClaim { get; set; }
    }

    public static class IApplicationBuilderExtensions
    {
        private static int _defaultPageSize = 10;

        private static void RegisterRepository<T, TRepo>(IApplicationBuilder app, Registration r, IRouteBuilder routeBuilder) where TRepo : IPagedRepository<T>
        {
            bool pagingEnabled = typeof(IPagedRepository<>).ImplementedBy(r.RepoType);
            Console.WriteLine($"IPagedRepository<> Implemented By {r.RepoType.Name} {pagingEnabled}");


            Console.WriteLine($"Registering: {r.RoutePrefix}/{{*id}}" + (pagingEnabled ? " <paged>" : null));

            foreach (MethodRegistration method in r.Methods)
            {
                Console.WriteLine($"\t{r.RoutePrefix}{method.Route}");

                routeBuilder.MapGet(r.RoutePrefix + method.Route, (HttpContext context) =>
                {
                    var provider = app.ApplicationServices;
                    var scopeFactory = provider.GetRequiredService<IServiceScopeFactory>();

                    using (var diScope = scopeFactory.CreateScope())
                    {
                        GenericStore<T, TRepo> store = (PagedStore<T, TRepo>)diScope.ServiceProvider.GetRequiredService(typeof(PagedStore<T, TRepo>));

                        context.Response.ContentType = "application/json";
                        string parameter = method.Method
                            .GetParameters()
                            .Select(p =>
                            {
                                StringValues val;
                                context.Request.Query.TryGetValue(p.Name, out val);
                                return val.SingleOrDefault();
                            }).SingleOrDefault();

                        if ( parameter == null)
                        {
                            throw new Exception("The queried file should be specified as a query string value. e.g. FindById => ?id=xxxx");
                        }
                        
                        string items = store.Find(r,
                            method.Method,
                            parameter);

                        if (items == null)
                        {
                            context.Response.StatusCode = 404;
                            return context.Response.WriteAsync(string.Empty);
                        }
                        return context.Response.WriteAsync(items);
                    }
                });
            }

            routeBuilder.MapRoute($"{r.RoutePrefix}/{{*id}}", (HttpContext context) =>
            {
                var provider = app.ApplicationServices;
                var scopeFactory = provider.GetRequiredService<IServiceScopeFactory>();

                using (var diScope = scopeFactory.CreateScope())
                {
                    try
                    {

                        if (r.AuthorizeConfig != null)
                        {
                            if (!context.User.Identities.Any(i => i.IsAuthenticated))
                            {
                                Console.WriteLine($"User not authenticated for {context.Request.Path}");
                                context.Response.StatusCode = 403;
                                return context.Response.WriteAsync("Access denied");
                            }
                            else
                            {
                                // else we are authenticated, let's try to check roles
                                if (r.AuthorizeConfig.Roles.Any())
                                {
                                    // if the user is in any of these roles
                                    if (r.AuthorizeConfig.Roles.Any(context.User.IsInRole))
                                    {
                                        //continue
                                    }
                                    else
                                    {

                                        Console.WriteLine($"User authenticated but not authorized for {context.Request.Path}");
                                        context.Response.StatusCode = 403;
                                        return context.Response.WriteAsync("Access denied");
                                    }
                                }
                                // else there are no roles, so we're just allowed in
                            }

                            // to apply scope to stores i.e. user = jim
                            // find scope attributes
                            if (r.ScopeConfig != null)
                            {
                                var scopeClaim = context.User.FindFirst(c => c.Type == r.ScopeConfig.ClaimType);
                                if (scopeClaim == null)
                                {
                                    string errorMessage = $"Request for {r.RepoType.Name} was scoped to {r.ScopeConfig.ClaimType} but not claim was provided.";
                                    Console.WriteLine(errorMessage);
                                    context.Response.StatusCode = 400;
                                    return context.Response.WriteAsync(errorMessage);
                                }

                                var requestContext = (RequestContext)diScope.ServiceProvider.GetRequiredService(typeof(RequestContext));
                                requestContext.ScopeClaim = scopeClaim;
                                requestContext.ScopeField = r.ScopeConfig.FieldName;
                            }
                            // pull out claim
                            // inject claim in to DI,
                            // maybe extend a context constructor object for stores

                        }

                        GenericStore<T, TRepo> store = (PagedStore<T, TRepo>)diScope.ServiceProvider.GetRequiredService(typeof(PagedStore<T, TRepo>));

                        context.Response.ContentType = "application/json";
                        string id = (string)context.GetRouteValue("id");
                        switch (context.Request.Method)
                        {
                            case "GET":
                                if (id == null)
                                {
                                    if (pagingEnabled)
                                    {
                                        PagingInfo pagingInfo = new PagingInfo
                                        {
                                            Page = context.Request.Query.ContainsKey("page") ? Int32.Parse(context.Request.Query["page"]) : 0,
                                            PageSize = context.Request.Query.ContainsKey("pageSize") ? Int32.Parse(context.Request.Query["pageSize"]) : _defaultPageSize
                                        };
                                        string pagedItems = ((PagedStore<T, TRepo>)store).GetAll(r, pagingInfo);
                                        return context.Response.WriteAsync(pagedItems);
                                    }
                                    string items = store.GetAll(r);
                                    return context.Response.WriteAsync(items);
                                }
                                string item = store.Get(r, (string)id);
                                if (item == null)
                                {
                                    context.Response.StatusCode = 404;
                                    return context.Response.WriteAsync(string.Empty);
                                }
                                return context.Response.WriteAsync(item);
                            case "POST":
                            case "PUT":
                                using (StreamReader bodyReader = new StreamReader(context.Request.Body))
                                {
                                    string location = store.Save(r, id, bodyReader.ReadToEnd());

                                    context.Response.Headers.Add("Location", location);
                                    context.Response.StatusCode = 201;
                                    return context.Response.WriteAsync(string.Empty);
                                }
                            case "DELETE":
                                string deletedItem = store.Delete(r, (string)id);

                                if (deletedItem == null)
                                {
                                    context.Response.StatusCode = 404;
                                    return context.Response.WriteAsync(string.Empty);
                                }
                                return context.Response.WriteAsync(deletedItem);
                        }
                    }
                    catch (Exception ex)
                    {
                        string eventGuid = Guid.NewGuid().ToString();
                        context.Response.StatusCode = 500;

                        if (ex.InnerException != null)
                        {
                            Console.WriteLine($"Unhandled exception: {ex.InnerException.Message}");
                            Console.WriteLine($"StackTrace: {ex.InnerException.StackTrace}");
                        }
                        else
                        {
                            Console.WriteLine($"Unhandled exception: {ex.Message}");
                            Console.WriteLine($"StackTrace: {ex.StackTrace}");
                        }

                        return context.Response.WriteAsync(eventGuid);
                    }
                    context.Response.StatusCode = 400;
                    return context.Response.WriteAsync("Unhandled HTTP verb");
                }
            });
        }

        public static void UseRapidRest(this IApplicationBuilder app)
        {
            IEnumerable<Registration> registrations = (IEnumerable<Registration>)app.ApplicationServices.GetService(typeof(IEnumerable<Registration>));
            RouteBuilder routeBuilder = new RouteBuilder(app);

            foreach (Registration r in registrations)
            {
                typeof(IApplicationBuilderExtensions)
                    .GetMethod("RegisterRepository", BindingFlags.NonPublic | BindingFlags.Static)
                    .MakeGenericMethod(r.ResourceType, r.RepoType)
                    .Invoke(null, new object[] { app, r, routeBuilder });
            }

            string indexPage = JsonConvert.SerializeObject(new
            {
                _links = registrations.ToDictionary(r => r.Name, r => $"http://localhost:5000/{r.RoutePrefix}")
            });

            routeBuilder.MapGet("api/v1", (HttpContext context) =>
            {
                return context.Response.WriteAsync(indexPage);
            });

            IRouter routes = routeBuilder.Build();
            app.UseRouter(routes);
        }
    }
}