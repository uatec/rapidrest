using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using RapidRest.Configuration;
using RapidRest.Eventing;
using RapidRest.HAL;
using RapidRest.Simple;

namespace RapidRest.Paging
{
     public class PagedStore<T, TRepo> : GenericStore<T, TRepo> where TRepo : IPagedRepository<T> {
        
        public PagedStore(TRepo repository, IEventEmitter<T> eventEmitter)
            :base(repository, eventEmitter)
        {
        }

        private IPagedRepository<T> Repository { 
            get {
                return (IPagedRepository<T>) _repository;
            }
        }

        public string GetAll(Registration registration, PagingInfo pagingInfo)
        {
            IEnumerable<T> response = this.Repository.GetAll(pagingInfo);
            int totalItemCount = int.MaxValue; // TODO: Get this from the data store
            var items = response.Select(r => {
                JObject obj = JObject.FromObject(r);
                string id = base.getId(r);
                obj["_links"] = JObject.FromObject(new HALLinks {
                    self =  new HALLink {
                        href = $"http://localhost:5000/{registration.RoutePrefix}/{id}"
                    }
                }, _jsonWriter);
                return obj;
            });

            HALResponse halResponse = new HALResponse {
                _links = new HALLinks {
                    prev = pagingInfo.Page > 0 ? new HALLink {
                        templated = true,
                        href = $"http://localhost:5000/{registration.RoutePrefix}?page={pagingInfo.Page-1}&pageSize={pagingInfo.PageSize}"
                    } : null,
                    next = totalItemCount > pagingInfo.PageSize ? new HALLink {
                        templated = true,
                        href = $"http://localhost:5000/{registration.RoutePrefix}?page={pagingInfo.Page+1}&pageSize={pagingInfo.PageSize}"
                    } : null
                },
                _embedded = items
            };

            return JObject.FromObject(halResponse, _jsonWriter).ToString();
        }
    }
}