using System.Collections.Generic;
using RapidRest.Simple;

namespace RapidRest.Paging
{
    public interface IPagedRepository<T>: IRepository<T>
    {
        IEnumerable<T> GetAll(PagingInfo pagingInfo);
    }
}