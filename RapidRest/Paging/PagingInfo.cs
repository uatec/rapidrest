namespace RapidRest.Paging
{
    public class PagingInfo
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}