using System;

namespace RapidRest.Eventing
{
    public interface IEventEmitter<T>
    {
        void Emit(IEvent<T> @event);
        void Register<TEvent>(Action<TEvent> handler) where TEvent : IEvent<T>;
    }
}