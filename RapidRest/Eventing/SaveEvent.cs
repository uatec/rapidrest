namespace RapidRest.Eventing
{
    public class BeforeSaveEvent<T> : IEvent<T>
    {
        public string Id { get; set; }
        public T Object { get; set; }
    }

    public class AfterSaveEvent<T> : IEvent<T>
    {
        public string Id { get; set; }
        public T Object { get; set; }
    }
}