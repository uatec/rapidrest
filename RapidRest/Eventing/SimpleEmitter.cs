using System;
using System.Collections.Generic;
using System.Reflection;

namespace RapidRest.Eventing
{
    public class SimpleEmitter<T> : IEventEmitter<T>
    {
        public class HandlerRegistration
        {
            public Action<IEvent<T>> Callback { get; set; }
            public Type EventType { get; set; }
        }

        IList<HandlerRegistration> _handlers = new List<HandlerRegistration>();

        public void Emit(IEvent<T> @event)
        {
            foreach ( HandlerRegistration handler in _handlers ) {
                if ( @event.GetType().GetTypeInfo().IsAssignableFrom(handler.EventType))
                {
                    handler.Callback(@event);
                }
            }
        }

        public void Register<TEvent>(Action<TEvent> handler) where TEvent : IEvent<T>
        {
            _handlers.Add(new HandlerRegistration {
                Callback = (IEvent<T> e) => handler((TEvent) e),
                EventType = typeof(TEvent)
            });
        }
    }
}