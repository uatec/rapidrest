namespace RapidRest.Eventing
{
    public class BeforeDeleteEvent<T> : IEvent<T>
    {
        public string Id { get; set; }
    }

    public class AfterDeleteEvent<T> : IEvent<T>
    {
        public string Id { get; set; }
        public T Object { get; set; }
    }
}