using System;
using System.Collections.Generic;

namespace RapidRest.Configuration
{
    public class Registration
    {
        public string Name { get; set; }
        public string RoutePrefix { get; set; }

        public Type RepoType { get; set; }
        public Type ResourceType { get; set; }
        public IEnumerable<MethodRegistration> Methods { get; set; }
        public Authorize AuthorizeConfig { get; set; }
        public ScopedTo ScopeConfig { get; set; }
    }
}