using System.Reflection;

namespace RapidRest.Configuration
{
    public class MethodRegistration 
    {
        public string Route { get; set; }
        public MethodInfo Method;
    }
}