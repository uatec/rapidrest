using System;
using System.Linq;
using System.Reflection;

namespace RapidRest
{

    /// <summary>
    /// Add this to your repository interface to require the user to have authenticated 
    /// </summary>
    public class Authorize : Attribute
    {
        private readonly string[] roles;

        public string[] Roles { get { return this.roles; } }

        public Authorize() {
            this.roles = new string[] {};
        }

        public Authorize(params string[] roles)
        {
            this.roles = roles;
        }
    }

    public class ScopedTo : Attribute
    {
        private readonly string claimType;
        private readonly string fieldName;

        public ScopedTo(string claimType, string fieldName) 
        {
            this.claimType = claimType;
            this.fieldName = fieldName;
        }

        public string ClaimType { get { return this.claimType; } }
        public string FieldName { get { return this.fieldName; } }
    }

    public static class TypeExtensions
    {
        public static MethodInfo GetDeepMethods(this Type type, string name)
        {
            var methodInfo = type.GetMethod(name);
            if ( methodInfo != null ) return methodInfo;

            if ( type.GetInterfaces().Length == 0 ) return null; 

            return type.GetInterfaces()[0].GetDeepMethods(name);
        }

        public static bool ImplementedBy(this Type generic, Type toCheck) {
            while (toCheck != null && toCheck != typeof(object)) {
                var cur = toCheck.GenericTypeArguments.Any() ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur) {
                    return true;
                }
                toCheck = toCheck.GetInterfaces().FirstOrDefault();
            }
            return false;
        }
    }
}