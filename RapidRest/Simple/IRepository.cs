using System.Collections.Generic;

namespace RapidRest.Simple
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        
        T Get(string id);
        
        string Save(string id, T obj);

        T Delete(string id);

        IEnumerable<T> FindBy(string key, object value);
        
        T FindOneBy(string key, object value);
    }
}