using System;
using System.Collections.Generic;
using Castle.DynamicProxy;
using RapidRest.Paging;

namespace RapidRest.Simple
{
    public class SimpleInterceptor<T> : IInterceptor
    {
        private readonly IPagedRepository<T> _dataRepository;

        public SimpleInterceptor(IPagedRepository<T> dataRepository) 
        {
            _dataRepository = dataRepository;
        }

        public void Intercept(IInvocation invocation)
        {
            if ( invocation.Method.Name == "Get") 
            {
                invocation.ReturnValue = _dataRepository.Get((string) invocation.Arguments[0]);
            }
            else if ( invocation.Method.Name == "Delete") 
            {
                invocation.ReturnValue = _dataRepository.Delete((string) invocation.Arguments[0]);
            } 
            else if ( invocation.Method.Name == "GetAll") 
            {
                if ( invocation.Arguments.Length == 1 ) 
                {
                    invocation.ReturnValue = _dataRepository.GetAll((PagingInfo) invocation.Arguments[0]);
                }
                else 
                {
                    invocation.ReturnValue = _dataRepository.GetAll();
                }
            }
            else if ( invocation.Method.Name == "Save") 
            {
                invocation.ReturnValue = _dataRepository.Save((string) invocation.Arguments[0], (T) invocation.Arguments[1]);
            }
            else if ( invocation.Method.Name.StartsWith("FindBy") ) 
            {
                if ( invocation.Method.ReturnType == typeof(IEnumerable<T>) )
                {
                    string key = invocation.Method.Name.Replace("FindBy", "");
                    invocation.ReturnValue = _dataRepository.FindBy(key, invocation.Arguments[0]);
                }
                else 
                {
                    string key = invocation.Method.Name.Replace("FindBy", "");
                    invocation.ReturnValue = _dataRepository.FindOneBy(key, invocation.Arguments[0]);
                }
            } 
            else 
            {
                throw new NotImplementedException();
            }
        }
    }
}