using System;
using Newtonsoft.Json;
using System.Reflection;
using Newtonsoft.Json.Linq;
using RapidRest.HAL;
using RapidRest.Configuration;
using System.Collections.Generic;
using System.Linq;
using RapidRest.Eventing;

namespace RapidRest.Simple
{    
    public class GenericStore<T, TRepo> where TRepo : IRepository<T> {
        protected JsonSerializer _jsonWriter = new JsonSerializer {
                                 NullValueHandling = NullValueHandling.Ignore
                             };

        protected readonly string IdFieldName;

        protected readonly IEventEmitter<T> _eventEmitter;
      
        protected readonly TRepo _repository;
        public GenericStore(TRepo repository, IEventEmitter<T> eventEmitter)
        {
            _repository = repository;
            IdFieldName = DiscoveryService.DiscoverIdFieldName(typeof(T));
            _eventEmitter = eventEmitter;
        }

        public virtual string GetAll(Registration registration) 
        {
            IEnumerable<T> response = _repository.GetAll();        
            var items = response.Select(r => {
                JObject obj = JObject.FromObject(r);
                string id = this.getId(r);
                obj["_links"] = JObject.FromObject(new HALLinks {
                    self =  new HALLink {
                        href = $"http://localhost:5000/{registration.RoutePrefix}/{id}"
                    }
                }, _jsonWriter);
                return obj;
            });

            HALResponse halResponse = new HALResponse {
                _embedded = items
            };

            return JObject.FromObject(halResponse, _jsonWriter).ToString();
        }

        protected string getId(object obj)
        {
            var idPropertyInfo = typeof(T).GetProperty(IdFieldName);
            if ( idPropertyInfo == null ) throw new Exception("Unable to find id property: " + IdFieldName);

            return (string) idPropertyInfo.GetValue(obj);
        }
        protected void setId(string id, object obj)
        {
            var idPropertyInfo = typeof(T).GetProperty(IdFieldName);
            if ( idPropertyInfo == null ) throw new Exception("Unable to find id property: " + IdFieldName);

            idPropertyInfo.SetValue(obj, id);
        }

        public string Save(Registration registration, string id, string obj)
        {
            T objInstance = JsonConvert.DeserializeObject<T>(obj);
            
            _eventEmitter.Emit(new BeforeSaveEvent<T> { Id = id, Object = objInstance});
                                
            var objectId = getId(objInstance);
            
            if ( id != null && objectId != null &&  id != objectId ) {
                throw new Exception("tried to change the ID of an object");
            }

            id = id ?? objectId ?? Guid.NewGuid().ToString();
            setId(id, objInstance);

            id = _repository.Save(id, objInstance);
            
            _eventEmitter.Emit(new AfterSaveEvent<T> { Id = id, Object = objInstance});

            return $"{registration.RoutePrefix}/{id}";
        }

        public string Get(Registration registration, string id)
        {
            T response = _repository.Get(id);
            if ( response == null ) return null;            
            JObject jobj = JObject.FromObject(response);
            jobj["_links"] = JObject.FromObject(new HALLinks {
                self =  new HALLink {
                    href = $"http://localhost:5000/{registration.RoutePrefix}/{id}"
                }
            }, _jsonWriter);
            return jobj.ToString();
        }

        public string Delete(Registration registration, string id)
        {
            _eventEmitter.Emit(new AfterDeleteEvent<T> { Id = id});

            T response = _repository.Delete(id);
            if ( response == null ) return null;
            
            _eventEmitter.Emit(new AfterDeleteEvent<T> { Id = id, Object = response });                

            return JsonConvert.SerializeObject(response);
        }

        public string Find(Registration restfulType, MethodInfo queryMethod, string queryValue)
        {
            object queryObject = JsonConvert.DeserializeObject(queryValue, queryMethod.GetParameters()[0].ParameterType);
            var response = queryMethod.Invoke(_repository, new object[] { queryObject });


            JToken jsonResponse = JToken.FromObject(response, _jsonWriter);

            if ( jsonResponse is JArray ) {

                JArray items = JArray.FromObject(response, _jsonWriter);
                foreach ( JObject obj in items) {
                    obj["_links"] = JObject.FromObject(new HALLinks {
                        self =  new HALLink {
                            href = $"http://localhost:5000/{restfulType.RoutePrefix}/{obj["Id"].Value<string>()}"
                        }
                    }, _jsonWriter);
                }
                HALResponse halResponse = new HALResponse {
                    _embedded = items
                };

                return JObject.FromObject(halResponse, _jsonWriter).ToString();
            }
            else if ( jsonResponse is JObject ) 
            {
                JObject jobj = (JObject) jsonResponse;
                jobj["_links"] = JObject.FromObject(new HALLinks {
                    self = new HALLink {
                        href = $"http://localhost:5000/{restfulType.RoutePrefix}/{jobj["Id"].Value<string>()}"
                    }
                }, _jsonWriter);
                return jobj.ToString();
            }
            
            throw new Exception("Unable to serialize response from IRepository");
        }
    }
}