namespace RapidRest.HAL
{
        public class HALLink
        {
            public string href { get; set; }
            public bool templated { get; set; }
        }
}