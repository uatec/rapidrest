namespace RapidRest.HAL 
{
    public class HALLinks 
    {
        public HALLink self { get; set; }
        public HALLink prev { get; set; }
        public HALLink next { get; set; }
    }
}