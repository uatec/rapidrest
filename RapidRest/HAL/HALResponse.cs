namespace RapidRest.HAL
{
    public class HALResponse 
    {
        public HALLinks _links;
        public object _embedded;
    }
}