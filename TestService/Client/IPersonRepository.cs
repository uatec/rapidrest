using System.Collections.Generic;
using RapidRest;
using RapidRest.Paging;

namespace TestService.Client
{
    [Authorize()]
    public interface IPersonRepository : IPagedRepository<Person> {
        IEnumerable<Person> FindByFirstName(string firstName);
        IEnumerable<Person> FindByLastName(string lastName);
        Person FindByAge(int age);
    }
}