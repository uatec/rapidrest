using RapidRest;
using RapidRest.Paging;

namespace TestService.Client
{
    [Authorize("user")]
    [ScopedTo("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", "OwnerName")]
    public interface ICatRepository : IPagedRepository<Cat> {
    }
}