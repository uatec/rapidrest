using Newtonsoft.Json;
using RapidRest.Paging;

namespace TestService.Client
{
    public class TaxRate 
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "rate")]
        public decimal Rate { get; set; }
    }
    
    public interface ITaxRateRepository : IPagedRepository<TaxRate>
    {

    }
}