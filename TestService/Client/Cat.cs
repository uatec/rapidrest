namespace TestService.Client
{
    public class Cat 
    {
        public string id { get; set; }
        public string Name { get; set; }
        public string OwnerName { get; set; }
    }
}