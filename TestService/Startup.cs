﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using RapidRest;
using RapidRest.Eventing;
using TestService.Client;
using System.Security.Claims;
using System.Reflection;

namespace TestService
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRapidRest(typeof(Startup).GetTypeInfo().Assembly);
        }

        public async Task AuthenticationStub(HttpContext context, Func<Task> next)
        {
            string userId = context.Request.Query["userid"];

            if ( !string.IsNullOrEmpty(userId)) {
                context.User.AddIdentity(new ClaimsIdentity(new Claim[] {   
                    new Claim("http://schemas.microsoft.com/ws/2008/06/identity/claims/role", "admin"),
                    new Claim("http://schemas.microsoft.com/ws/2008/06/identity/claims/role", "user"),
                    new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", userId)
                }, "some authentication type"));
            }

            await next.Invoke();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.Use(AuthenticationStub);

            app.UseRapidRest();

            var personEventEmitter = (IEventEmitter<Person>) app.ApplicationServices.GetService(typeof(IEventEmitter<Person>));

            personEventEmitter.Register((BeforeSaveEvent<Person> e) => {
                Console.WriteLine(e.Id + " BeforeSaveEvent");
            });
            personEventEmitter.Register((AfterSaveEvent<Person> e) => {
                Console.WriteLine(e.Id + " AfterSaveEvent");
            });
            personEventEmitter.Register((BeforeDeleteEvent<Person> e) => {
                Console.WriteLine(e.Id + " BeforeDeleteEvent");
            });
            personEventEmitter.Register((AfterDeleteEvent<Person> e) => {
                Console.WriteLine(e.Id + " AfterDeleteEvent");
            });
        }
    }
}